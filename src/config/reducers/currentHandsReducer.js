import * as Types from "../actionTypes";

const initializeState = [];

const currentHandsReducer = (state = initializeState, action) => {
  switch (action.type) {
    case Types.ADD_HANDS_TO_JUDGE:
      return [...action.payload];

    default:
      return state;
  }
};

export default currentHandsReducer;
