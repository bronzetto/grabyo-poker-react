import * as Types from "../actionTypes";

const initializeState = [];

const playersReducer = (state = initializeState, action) => {
  switch (action.type) {
    case Types.ADD_PLAYER:
      return [...state, action.payload];

    case Types.REMOVE_PLAYER:
      return [...state.filter(player => player.id !== action.payload)];

    case Types.EDIT_PLAYER:
      return [
        ...state.map(player => {
          if (player.id === action.payload.id) {
            return {
              ...player,
              name: action.payload.newName
            };
          } else {
            return player;
          }
        })
      ];

    case Types.ADD_CURRENT_HAND:
      return [
        ...state.map(player => {
          if (player.id === action.payload.id) {
            return {
              ...player,
              currentHand: action.payload.currentHand
            };
          } else {
            return player;
          }
        })
      ];

    case Types.UPDATE_SCORE:
      return [
        ...state.map(player => {
          return player.currentHand.includes(...action.payload)
            ? {
                ...player,
                winnedHands: player.winnedHands + 1,
                isCurrentHandWinning: true
              }
            : {
                ...player,
                isCurrentHandWinning: false
              };
        })
      ];

    default:
      return state;
  }
};

export default playersReducer;
