import * as Types from "../actionTypes";

const initializeState = [];

const deckReducer = (state = initializeState, action) => {
  switch (action.type) {
    case Types.CREATE_DECK:
      return [...action.payload];

    default:
      return state;
  }
};

export default deckReducer;
