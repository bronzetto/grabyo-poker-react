import * as Types from "./actionTypes";

//  Player actions
export const addNew = player => {
  return {
    type: Types.ADD_PLAYER,
    payload: player
  };
};

export const removePlayer = id => {
  return {
    type: Types.REMOVE_PLAYER,
    payload: id
  };
};

export const editPlayer = (id, newName) => {
  return {
    type: Types.EDIT_PLAYER,
    payload: { id, newName }
  };
};

export const addCurrentHand = (currentHand, id) => {
  return {
    type: Types.ADD_CURRENT_HAND,
    payload: { currentHand, id }
  };
};

export const updateScore = winnedHand => {
  return {
    type: Types.UPDATE_SCORE,
    payload: winnedHand
  };
};

// Deck actions
export const createDeck = deck => {
  return { type: Types.CREATE_DECK, payload: deck };
};

export const addHandsToJudge = hands => {
  return {
    type: Types.ADD_HANDS_TO_JUDGE,
    payload: hands
  };
};
