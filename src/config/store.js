import { createStore, combineReducers } from "redux";
import playerReducer from "./reducers/playersReducer";
import deckReducer from "./reducers/deckReducer";
import currentHandsReducer from "./reducers/currentHandsReducer";

const initialState = {};

const rootReducer = combineReducers({
  players: playerReducer,
  deck: deckReducer,
  currentHands: currentHandsReducer
});

const store = createStore(
  rootReducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
