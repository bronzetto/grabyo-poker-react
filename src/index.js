import React from "react";
import ReactDOM from "react-dom";
import "./Styles/global.css";
import App from "./Components/App";

// import { Provider } from "react-redux";
// import store from "./config/store";

import ReduxProvider from "./ReduxProvider";

ReactDOM.render(
  <ReduxProvider>
    <App />
  </ReduxProvider>,
  document.getElementById("root")
);
