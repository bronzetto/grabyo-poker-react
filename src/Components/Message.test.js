import React, { Component } from "react";
import Message from "./Message";
import { mount, shallow } from "enzyme";

describe("<Message />", () => {
  it("renders a message when less than 2 players", () => {
    let wrapped = shallow(<Message players={1} />);
    expect(
      wrapped
        .find("h1")
        .render()
        .text()
    ).toEqual(
      "You need to add at least two players to be able to play and not more than six players are allowed."
    );
  });

  it("renders a message when 6 players", () => {
    let wrapped = shallow(<Message players={6} />);
    expect(
      wrapped
        .find("h1")
        .render()
        .text()
    ).toEqual("No more than six players are allowed.");
  });

  it("renders no message when players are between 2 and 6", () => {
    let wrapped = shallow(<Message players={4} />);
    expect(wrapped.find("h1").length).toEqual(0);
  });
});
