import React, { Component } from "react";

import Button from "./Button";

import { StyledInput } from "../Styles/Styled";

class NewUserInput extends Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.state = {
      inputText: ""
    };
    this.handleDispatchUser = this.handleDispatchUser.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.isActive) {
      setTimeout(() => {
        this.textInput.current.focus();
      }, 100);
    }
  };

  handleInputChange(e) {
    this.setState({ inputText: e.target.value });
  }

  handleDispatchUser() {
    let userName = this.state.inputText;
    let playerId = this.props.playerId;
    if (userName.length > 0) this.props.addUserName(userName, playerId);
    this.setState({ inputText: "" });
  }

  render() {
    const { isActive, children } = this.props;
    return (
      isActive && (
        <>
          <StyledInput
            placeholder="User Name"
            ref={this.textInput}
            onChange={e => this.handleInputChange(e)}
          />
          <Button icon="💾" isClicked={() => this.handleDispatchUser()}>
            {children}
          </Button>
        </>
      )
    );
  }
}

export default NewUserInput;
