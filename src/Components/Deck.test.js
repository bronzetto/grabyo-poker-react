import { mount } from "enzyme";

import React from "react";

import Deck from "./Deck";
import { Card } from "../Styles/Styled";

import { suits, values } from "../utils";

describe(`Card deck`, () => {
  test("renders the right amount of cards", () => {
    const d = suits.map(suit => values.map(value => suit + value));
    const deck = mount(<Deck suits={suits} values={values} currentDeck={d} />);
    expect(deck.find(Card)).toHaveLength(52);
    deck.unmount();
  });
});
