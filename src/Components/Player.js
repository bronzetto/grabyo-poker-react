import React from "react";

import Button from "./Button";

import { Card, PlayerHand, Winner } from "../Styles/Styled";

const Player = ({ name, fireEdit, fireRemove, score, hand, isWinning }) => (
  <article>
    <p>
      {name}
      <Button icon="✏️" isClicked={fireEdit}>
        Edit
      </Button>
      <Button icon="🔥" isClicked={fireRemove}>
        Remove
      </Button>
      Winned Hands: <span>{score}</span>
    </p>

    <PlayerHand>
      {hand && hand.length > 0 ? (
        hand.map(card => {
          let suit = card.split("")[0];
          let value = card.split("")[1];
          return (
            <Card suit={suit} value={value} key={suit + card}>
              {value}
            </Card>
          );
        })
      ) : (
        <h2>player has no cards...</h2>
      )}
      {isWinning ? <Winner>{name} wins this hand!!!</Winner> : null}
    </PlayerHand>
  </article>
);

export default Player;
