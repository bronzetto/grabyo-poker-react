import React, { Fragment } from "react";
import { Card, StyledDeck } from "../Styles/Styled";

const Deck = ({ suits, values, currentDeck }) => (
  <StyledDeck>
    {suits.map(suit => (
      <Fragment key={suit}>
        {values.map(value => {
          let isInHandCard;
          currentDeck.includes(suit + value)
            ? (isInHandCard = true)
            : (isInHandCard = false);
          return (
            <Card
              key={suit + value}
              suit={suit}
              value={value}
              selected={isInHandCard}
            >
              {value}
            </Card>
          );
        })}
      </Fragment>
    ))}
  </StyledDeck>
);

export default Deck;
