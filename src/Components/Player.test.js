import React, { Component } from "react";
import { mount } from "enzyme";

import Player from "./Player";
import { Card, PlayerHand, Winner } from "../Styles/Styled";
import Button from "./Button";

describe("<Player />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<Player hand={["HA", "CQ", "S9", "S8", "H9"]} />);
  });
  afterEach(() => {
    wrapped.unmount();
  });
  it("renders a button edit, a button remove and a player hand component", () => {
    expect(wrapped.find(Button).length).toEqual(2);
    expect(wrapped.find(PlayerHand).length).toEqual(1);
  });

  it("renders the current hand", () => {
    expect(wrapped.props().hand).toEqual(["HA", "CQ", "S9", "S8", "H9"]);
    expect(wrapped.find(Card).length).toEqual(5);
  });

  it("renders a message if no cards are given", () => {
    wrapped = mount(<Player hand={[]} />);
    expect(wrapped.props().hand).toEqual([]);
    expect(wrapped.find(Card).length).toEqual(0);
    expect(wrapped.find("h2").text()).toEqual("player has no cards...");
  });

  it("renders a Winning message if player wins", () => {
    wrapped = mount(
      <Player hand={["HA", "CQ", "S9", "S8", "H9"]} isWinning={true} />
    );
    expect(wrapped.props().isWinning).toEqual(true);
    expect(wrapped.find(Card).length).toEqual(5);
    expect(wrapped.find(Winner).text()).toContain("wins this hand!!!");
  });
});
