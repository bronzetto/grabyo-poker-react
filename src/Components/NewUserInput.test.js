import React, { Component } from "react";
import NewUserInput from "./NewUserInput";
import { StyledInput } from "../Styles/Styled";
import Button from "./Button";
import { mount, shallow } from "enzyme";

describe("<NewUserInput />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = shallow(
      <NewUserInput player={4} isActive={true}>
        Add a Goblin!
      </NewUserInput>
    );
  });
  it("renders a an input and a button", () => {
    expect(wrapped.find(Button).length).toEqual(1);
    expect(wrapped.find(StyledInput).length).toEqual(1);
  });

  it("renders the passed text as children prop", () => {
    expect(wrapped.find(Button).props().children).toEqual("Add a Goblin!");
  });
});
