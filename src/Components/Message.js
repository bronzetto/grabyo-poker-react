import React from "react";

const Message = ({ players }) => {
  let message;

  players < 2
    ? (message = (
        <h1>
          You need to add at least two players to be able to play and not more
          than six players are allowed.
        </h1>
      ))
    : players >= 6
    ? (message = <h1>No more than six players are allowed.</h1>)
    : (message = "");

  return message;
};

export default Message;
