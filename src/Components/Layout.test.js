import React from "react";
import { shallow } from "enzyme";

import Layout from "./Layout";
import { Main, Header } from "../Styles/Styled";

describe("<Layout />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = shallow(<Layout />);
  });
  afterEach(() => {
    wrapped.unmount();
  });
  it("renders a header component", () => {
    expect(wrapped.find(Header).length).toEqual(1);
  });

  it("renders a Main component", () => {
    expect(wrapped.find(Main).length).toEqual(1);
  });
});
