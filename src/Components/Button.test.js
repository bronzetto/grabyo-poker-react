import React, { Component } from "react";
import { StyledButton } from "../Styles/Styled";
import Button from "./Button";
import { mount, shallow } from "enzyme";

describe("<Button />", () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<Button>Click</Button>);
  });
  afterEach(() => {
    wrapped.unmount();
  });
  it("renders a StyledButton ", () => {
    expect(wrapped.find(StyledButton).length).toEqual(1);
  });

  it("renders the passed text as children prop", () => {
    expect(wrapped.props().children).toEqual("Click");
  });
});
