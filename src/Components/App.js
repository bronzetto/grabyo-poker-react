import React, { Component } from "react";
import uuidV4 from "uuid/v4";
import poker from "poker-hands";
import { connect } from "react-redux";
import {
  addNew,
  removePlayer,
  editPlayer,
  createDeck,
  addCurrentHand,
  addHandsToJudge,
  updateScore
} from "../config/actionCreators";

import { suits, values, getShuffledDeck } from "../utils";

import Layout from "./Layout";
import Deck from "./Deck";
import Player from "./Player";
import Button from "./Button";
import Message from "./Message";

import NewUserInput from "./NewUserInput";

import { Footer } from "../Styles/Styled";

class App extends Component {
  state = {
    isAddPlayerActive: false,
    editPlayer: {}
  };

  componentDidMount() {
    if (this.props.players.length < 2)
      this.setState({ isAddPlayerActive: true });
  }

  newDeck() {
    let shuffledDeck = getShuffledDeck();
    this.props.createDeck(shuffledDeck);
  }

  handleAddNewPlayer(name) {
    const id = uuidV4();
    let player = {
      name,
      id,
      winnedHands: 0,
      currentHand: [],
      isCurrentHandWinning: false
    };
    this.props.addNew(player);
    this.setState({ isAddPlayerActive: false });
  }

  showAddUserInput() {
    this.setState({ isAddPlayerActive: true });
  }

  handleRemovePlayer(id) {
    this.props.removePlayer(id);
    this.resetState();
  }

  handleActivateEditPlayer(id) {
    this.setState({ editPlayer: { [id]: true } });
  }

  handleEditNewPlayer(newName, playerId) {
    this.props.editPlayer(playerId, newName);
    this.resetState();
    this.setState({ editPlayer: { [playerId]: false } });
  }

  handleJudgeWinner() {
    this.newDeck();

    setTimeout(() => {
      let deck = this.props.deck;
      let players = this.props.players;
      let judged;
      let currentHands = players.map(player => {
        let playerHand = deck.splice(-5, 5);

        this.props.addCurrentHand(playerHand, player.id);

        return playerHand.join(" ");
      });

      this.props.addHandsToJudge(currentHands);

      judged = poker.judgeWinner(currentHands);

      this.props.updateScore(currentHands[judged].split(" "));
    }, 0);
  }

  resetState() {
    this.setState({
      isAddPlayerActive: false,
      editPlayerId: null
    });
  }

  render() {
    const { players } = this.props;
    const { isAddPlayerActive, isEditPlayerActive } = this.state;

    return (
      <Layout>
        <section>
          <h1>Cards deck</h1>
          <Deck suits={suits} values={values} currentDeck={this.props.deck} />
        </section>
        <section>
          <header>
            <h1>Players</h1>
          </header>
          <section>
            <Message players={players.length} />

            {players.map(player => (
              <div key={player.id}>
                <hr />
                <hr />
                <NewUserInput
                  isActive={this.state.editPlayer[player.id] ? true : false}
                  addUserName={this.handleEditNewPlayer.bind(this)}
                  playerId={player.id}
                >
                  Replace
                </NewUserInput>
                <br />
                <Player
                  name={player.name}
                  id={player.id}
                  hand={player.currentHand}
                  isWinning={player.isCurrentHandWinning}
                  score={player.winnedHands}
                  fireRemove={() =>
                    this.handleRemovePlayer.bind(this)(player.id)
                  }
                  fireEdit={() =>
                    this.handleActivateEditPlayer.bind(this)(player.id)
                  }
                />
                <br />
              </div>
            ))}
          </section>
          <Footer>
            <NewUserInput
              isActive={isAddPlayerActive}
              addUserName={this.handleAddNewPlayer.bind(this)}
            >
              Add
            </NewUserInput>

            {players.length < 6 ? (
              <Button icon="🙋‍♀️" isClicked={this.showAddUserInput.bind(this)}>
                Add new player
              </Button>
            ) : (
              <Button
                icon="🙋‍♀️"
                isClicked={this.showAddUserInput.bind(this)}
                isDisable={true}
              >
                Add new player
              </Button>
            )}
            {players.length < 2 || players.length > 6 ? (
              <Button icon="🏆" isDisable={true}>
                Find the winner
              </Button>
            ) : (
              <Button icon="🏆" isClicked={this.handleJudgeWinner.bind(this)}>
                Find the winner
              </Button>
            )}
          </Footer>
        </section>
      </Layout>
    );
  }
}

const mapStateToProps = state => state;

const mapDispatchToProps = state => dispatch => {
  return {
    addNew: player => dispatch(addNew(player)),
    removePlayer: id => dispatch(removePlayer(id)),
    editPlayer: (id, newName) => dispatch(editPlayer(id, newName)),
    createDeck: deck => dispatch(createDeck(deck)),
    updateScore: winnedHand => dispatch(updateScore(winnedHand)),
    addHandsToJudge: hands => dispatch(addHandsToJudge(hands)),
    addCurrentHand: (currentHand, id) =>
      dispatch(addCurrentHand(currentHand, id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
