export const values = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "T",
  "J",
  "Q",
  "K",
  "A"
];
export const suits = ["D", "H", "S", "C"];

export const getColourForSuit = suit =>
  suit === "D" || suit === "H" ? "red" : "black";

export const generateFullDeck = suits.map(suit =>
  values.map(value => suit + value)
);

export const shuffle = deck => {
  for (let i = deck.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [deck[i], deck[j]] = [deck[j], deck[i]];
  }
  return deck;
};

export const getShuffledDeck = deck =>
  shuffle(shuffle(generateFullDeck.flat()));
